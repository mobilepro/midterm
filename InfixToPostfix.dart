import 'dart:io';
import 'Tokenizing.dart';

checkOperator(String operators) {
  if (operators != "(" &&
      operators != ")" &&
      operators != "+" &&
      operators != "-" &&
      operators != "*" &&
      operators != "/" &&
      operators != "^") {
    return 0;
  } else if (operators == "+" ||
      operators == "-" ||
      operators == "*" ||
      operators == "/" ||
      operators == "^") {
    return 1;
  } else if (operators == "(") {
    return 2;
  } else if (operators == ")") {
    return 3;
  }
}

List InfixTiPostfix(String expression) {
  List listToken = tokenizing(expression);

  List listOperators = [];
  List listPostfix = [];

  int precedence = 0;
  int precedenceOper = 0;

  for (int i = 0; i < listToken.length; i++) {
    if (checkOperator(listToken[i]) == 0) {
      listPostfix.add(listToken[i]);
    } //else
    if (checkOperator(listToken[i]) == 1) {
      switch (listToken[i]) {
        //precedence=1
        case "+":
          {
            precedence = 1;
          }
          break;
        case "-":
          {
            precedence = 1;
          }
          break;

        //precedence=2
        case "*":
          {
            precedence = 2;
          }
          break;
        case "/":
          {
            precedence = 2;
          }
          break;

        //precedence=3
        case "^":
          {
            precedence = 2;
          }
          break;
      }
      if (listOperators.isNotEmpty) {
        switch (listOperators[listOperators.length - 1]) {
          //precedenceOper=1
          case "+":
            {
              precedenceOper = 1;
            }
            break;
          case "-":
            {
              precedenceOper = 1;
            }
            break;

          //precedenceOper=2
          case "*":
            {
              precedenceOper = 2;
            }
            break;
          case "/":
            {
              precedenceOper = 2;
            }
            break;

          //precedenceOper=3
          case "^":
            {
              precedenceOper = 2;
            }
            break;
        }
      }
      while (listOperators.isNotEmpty &&
          listOperators[listOperators.length - 1] != "(" &&
          precedence <= precedenceOper) {
        // print(precedence);
        //  print(precedenceOper);
        listPostfix.add(listOperators[listOperators.length - 1]);
        listOperators.removeAt(listOperators.length - 1);
        if (listOperators.isNotEmpty) {
          switch (listOperators[listOperators.length - 1]) {
            //precedenceOper=1
            case "+":
              {
                precedenceOper = 1;
              }
              break;
            case "-":
              {
                precedenceOper = 1;
              }
              break;

            //precedenceOper=2
            case "*":
              {
                precedenceOper = 2;
              }
              break;
            case "/":
              {
                precedenceOper = 2;
              }
              break;

            //precedenceOper=3
            case "^":
              {
                precedenceOper = 2;
              }
              break;
          }
        }
      }
      listOperators.add(listToken[i]);
    } //else
    if (checkOperator(listToken[i]) == 2) {
      listOperators.add(listToken[i]);
    } //else
    if (checkOperator(listToken[i]) == 3) {
      // operator.remove();

      while (listOperators[listOperators.length - 1] != "(") {
        listPostfix.add(listOperators[listOperators.length - 1]);
        listOperators.removeAt(listOperators.length - 1);
      }

      for (int j = 0; j < listOperators.length; j++) {
        if (listOperators[j] == "(") {
          listOperators.removeAt(j);
        }
      }
    }
  }

  while (listOperators.isNotEmpty) {
    listPostfix.add(listOperators[listOperators.length - 1]);
    listOperators.removeAt(listOperators.length - 1);
  }
  return listPostfix;
}

void main(List<String> args) {
  print("Please input mathematical expressions : ");
  String? expression = stdin.readLineSync()!;
  print("\nPostfix as the result of the algorithm : ");
  print(InfixTiPostfix(expression));
  print("");
}
