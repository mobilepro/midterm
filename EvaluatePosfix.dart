import 'dart:io';
import 'dart:math';

import 'InfixToPostfix.dart';

evaluatePostfix(String expression){
  List listInfixToPostfix = InfixTiPostfix(expression);
  List<double> listValues = [];

  double result = 0;

  for (int i = 0; i < listInfixToPostfix.length; i++) {
    if (checkOperator(listInfixToPostfix[i]) == 0) {
      listValues.add(double.parse(listInfixToPostfix[i]));
    } else {
      double right = listValues.last;
      listValues.remove(listValues.last);
      double left = listValues.last;
      listValues.remove(listValues.last);

      switch (listInfixToPostfix[i]) {
        case "+":
          {
            result = left + right;
          }
          break;
        case "-":
          {
            result =  left - right;
          }
          break;
        case "*":
          {
            result =  left * right;
          }
          break;
        case "/":
          {
            result =  left / right;
          }
          break;
        case "^":
          {
            result = 0.0 + pow( left ,right);
          }
          break;
      }

      listValues.add(result);
    }
  }
  return listValues[0];
}

void main(List<String> args) {
  print("Please input mathematical expressions : ");
  String? expression = stdin.readLineSync()!;

  print("\nResult : ");
  print(evaluatePostfix(expression));
  print("");

}
